'use strict';

let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let jwt = require('jsonwebtoken');
let config = require('./config');
let User = require('./app/models/user');
let apiRoutes = express.Router();
let url = require('url');

let port = process.env.PORT || 8080;
mongoose.connect(config.database);
app.set('secret', config.secret);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

apiRoutes.post('/login', (req, res) => {
  if (typeof req.body.email === 'undefined' || 
  	  typeof req.body.password === 'undefined') {
  	res.status(400).send('incorrect data');
  } else {
    User.findOne({
      email: req.body.email
    }, (err, user) => {
      if (err) throw err;
      if (!user) {
        res.json({ success: false, message: 'Login failed. User not found.' });
      } else if (user) {
        if (user.password != req.body.password) {
          res.json({ success: false, message: 'Login failed. Wrong password.' });
        } else {
          let token = jwt.sign(user, app.get('secret'));
          user.token = token;
          user.save();
          res.json({
            success: true,
            message: 'Login success!',
            token: token
          });
        }   
      }
    });
  }
});

apiRoutes.post('/register', (req, res) => {
  let regData = new User({
	name: req.body.name,
	password: req.body.password,
	email: req.body.email
  });
  regData.save((err) => {
    if (err) {
    	res.status(400).send('invalid data');
    } else {
      User.findOne({
        email: regData.email
      }, (err, user) => {
        if (err) throw err;
        if (!user) {
          res.json({ success: false, message: 'Registration failed' });
        } else if (user) {
          let token = jwt.sign(user, app.get('secret'));
          user.token = token;
          user.save();
          res.status(201);
          res.json({
            success: true,
            message: 'Registration success!',
            token: token
          });
        }   
      });
    }
  });
});

apiRoutes.get('/logout', (req, res) => {
  if (typeof req.headers.authorization === 'undefined'){
  	res.status(401).send('not authorized');
  } else {
  	let _token = req.headers.authorization.split(' ')[1];
    User.findOne({
   	  token: _token
    }, (err, user) => {
  	  if (err) throw err;
  	  if (!user) {
  	    res.status(401).send('not authorized');
  	  } else {
  	    user.token = undefined;
  	    user.save();
  	    res.status(200).send();
  	  }
    });
  }
});

apiRoutes.get('/profile', (req, res) => {
  if (typeof req.headers.authorization === 'undefined'){
  	res.status(401).send('not authorized');
  } else {
    let _token = req.headers.authorization.split(' ')[1];
    User.findOne({
   	  token: _token
    }, (err, user) => {
  	  if (err) throw err;
  	  if (!user) {
  	    res.status(401).send('not authorized');
  	  } else {
  	    res.json({
  	  	  name: user.name,
  	  	  email: user.email
  	    });
  	  }
    });
  }
})

app.use('/api', apiRoutes);
app.listen(port);
console.log('Server started at http://localhost:' + port);

module.exports = app;